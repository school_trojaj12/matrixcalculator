FROM debian:bullseye-slim as build

RUN apt update && apt install g++ make -y

RUN mkdir /app
COPY . /app

RUN make -C /app MatrixCalculator

FROM debian:bullseye-slim

RUN mkdir /app

COPY --from=build /app/MatrixCalculator /app/MatrixCalculator

ENTRYPOINT [ "/app/MatrixCalculator" ]